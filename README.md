# Nuke Blink Scripts

## Introduction

This repository contains of all BlinkScript code I developed and gathered throughout 2016-2019, while discovering and learning the principles of this cool, shader-like language available in Nuke.

Huge part of BlinkScript learning process was to port many GLSL shaders from [ShaderToy](https://www.shadertoy.com) and [GLSL Sandbox](http://glslsandbox.com) and figuring out the differences between both languages. While every script here was coded by hand, I believe it should be possible to develop a neat translator/converter for porting GLSL shaders directly into BlinkScript syntax.

For those interested in using raw GLSL shaders in Nuke, I cannot recommend more a brilliant plugin called [Nuke GLSL Shader](https://gitlab.com/hendrikproosa/nuke-glsl-shader), developed by Hendrik Proosa.

If you spot any bugs, feel free to create an Issue here on GitLab.

## Demo

Some of the glitch BlinkScripts are featured in this video:

[Blink glitch gizmos](https://vimeo.com/222667091)

## List of scripts

Detailed description of each script is coming up! Stay tuned!

- binary_glitch.blink
- camera_projection.blink
- curvature.blink
- degrader.blink
- edge_detection.blink
- env_reflection.blink
- env_refraction.blink
- falloff.blink
- fbm_3d_noise.blink
- kuwahara_filter.blink
- lensflare.blink
- mpeg_glitch.blink
- relight.blink
- simplex_noise.blink
- ssao.blink
- ssgi.blink
- tiled_projection.blink
- tiled_stmap.blink
- triplanar.blink
- vhs_distortion.blink
- worley_2d_noise.blink
- worley_3d_noise.blink

To achieve animated result, some of the kernels have `time` parameter exposed. Feeding it with `=frame` TCL expression should result in evolving effect.

## References

Sources for some snippets are mentioned where it applies. Sorry, if I couldn't point all of the reference material I used while creating those scripts - my memory could be fading at this point, as I'm no longer active in developing Blink kernels.

