// TriPlanar projection with some basic blending

kernel triPlanar : ImageComputationKernel<ePixelWise>
{
    Image<eRead, eAccessRandom, eEdgeConstant> worldP;
    Image<eRead, eAccessRandom, eEdgeConstant> worldN;
    Image<eRead, eAccessRandom, eEdgeConstant> src;
    Image<eWrite> dst;  //the output image

    param:
        float scale;
        float sharpness;

    local:
        int src_height;
        int src_width;

    void define() {
        defineParam(scale, "Scale", 1.0f);
        defineParam(sharpness, "Sharpness", 1.0f);
    }

    //The init() function is run before any calls to process().
    void init() {
        src_height = src.bounds.y2 - src.bounds.y1;
        src_width = src.bounds.x2 - src.bounds.x1;
    }

    void process( int2 pos ) {

        float4 value;

        if ( worldP.kComps > 2 ) {
            const float wx = worldP(pos.x, pos.y, 0);
            const float wy = worldP(pos.x, pos.y, 1);
            const float wz = worldP(pos.x, pos.y, 2);
            float3 P = normalize(float3( wx, wy, wz ) );

            const float nx = worldN(pos.x, pos.y, 0);
            const float ny = worldN(pos.x, pos.y, 1);
            const float nz = worldN(pos.x, pos.y, 2);
            float3 N = normalize(float3( nx, ny, nz ));

            float x = (wx) / scale;
            float y = (wy) / scale;
            float z = (wz) / scale;

            float2 yUV = float2(x, z);
            float2 xUV = float2(z, y);
            float2 zUV = float2(x, y);

            int ypx = (yUV.x+1.0f)/2.0f * src_width;
            int ypy = (yUV.y+1.0f)/2.0f * src_height;
            int xpx = (xUV.x+1.0f)/2.0f * src_width;
            int xpy = (xUV.y+1.0f)/2.0f * src_height;
            int zpx = (zUV.x+1.0f)/2.0f * src_width;
            int zpy = (zUV.y+1.0f)/2.0f * src_height;

            float4 ySrc = src(ypx, ypy);
            float4 xSrc = src(xpx, xpy);
            float4 zSrc = src(zpx, zpy);

            float3 blendWeights;
            blendWeights.x += pow(fabs(N.x), sharpness);
            blendWeights.y += pow(fabs(N.y), sharpness);
            blendWeights.z += pow(fabs(N.z), sharpness);
            blendWeights = blendWeights / (blendWeights.x + blendWeights.y + blendWeights.z);

            value = xSrc * blendWeights.x + ySrc * blendWeights.y + zSrc * blendWeights.z;

            dst() = value;
        } else {
            dst() = value;
        }
    }
};




