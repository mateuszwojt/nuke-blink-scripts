// Curvature shader based on Normal and Position pass
// Ported from: http://madebyevan.com/shaders/curvature

inline float dFd(float3 pixelA, float3 pixelB) {
	float La = 0.299f*pixelA.x + 0.587f*pixelA.y + 0.114f*pixelA.z;
	float Lb = 0.299f*pixelB.x + 0.587f*pixelB.y + 0.114f*pixelB.z;

	return La-Lb;
}

kernel Curvature : ImageComputationKernel<ePixelWise>
{
    Image<eRead, eAccessRandom, eEdgeConstant> N;
    Image<eRead, eAccessRandom, eEdgeConstant> P;
    Image<eWrite> dst;  //the output image

    param:
        float strength;
        float depth;
        float min_fresnel;
        float fresnel_gamma;

    local:

    void define() {
        defineParam(strength, "Strength", 4.0f);
        defineParam(depth, "Depth", 1.0f);
        defineParam(min_fresnel, "MinFresnel", 0.0f);
        defineParam(fresnel_gamma, "FresnelGamma", 1.0f);
    }

    void process(int2 pos) {
        float result = 0.0f;

        float nx = N( pos.x, pos.y, 0 );
        float ny = N( pos.x, pos.y, 1 );
        float nym = N( pos.x, pos.y-1, 1);
        float nyp = N( pos.x, pos.y+1, 1);
        float nz = N( pos.x, pos.y, 2 );


        float px = P( pos.x, pos.y, 0 );
        float py = P( pos.x, pos.y, 1 );
        float pz = P( pos.x, pos.y, 2 );

        float3 n = float3( nx, ny, nz );
        float3 nmin = float3( nx, nym, nz );
        float3 nplus = float3( nx, nyp, nz );
        float3 p = float3( px, py, pz );

        float3 nn = normalize( n );
        float3 pp = normalize( p );

        float3 dx = dFd(nn, nmin);
        float3 dy = dFd(nn, nplus);
        float3 xneg = nn - dx;
        float3 xpos = nn + dx;
        float3 yneg = nn - dy;
        float3 ypos = nn + dy;

        float curvature = (cross(xneg, xpos).y - cross(yneg, ypos).x) * strength * 80.0f / depth;

        float fresnel = 1.0f;
        fresnel = pow( 1.0f-fabs(dot(nn, pp)), fresnel_gamma );
        fresnel = fresnel*(1.0f-min_fresnel)+min_fresnel;

        result = clamp(curvature * fresnel, 0.0f, 1.0f);

        dst() = result;

    }

};
