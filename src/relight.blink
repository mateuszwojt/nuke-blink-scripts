// Relighting based on Oren-Nayar and Cook-Torrance shading/reflection models

inline float3 faceforward( float3 N, float3 I, float3 Ng) {
    float3 ret = dot(I, Ng) < 0 ? N : -N;
    return ret;
}

inline float Bias(float t, float a) {
    return pow(t, -(log(a)/log(2.0f)));
}

inline float schlickFresnel(float3 P, float3 N, float3 I, float bias, float eta, float Kfr) {
    float3 Nn = normalize(N);
    float3 Vn = -normalize(I);

    float dotnv = fabs(dot(Nn, Vn));
    float Kr = eta + (1.0f - eta) * pow(1.0f - dotnv, 5.0f);
    Kr = Kfr * Bias(Kr, bias);

    float color;
    color += Kr;

    return clamp(color, 0.0f, 59999.0f);
}

inline float orenNayar(float3 P, float3 N, float3 V, float3 L, float Kd, float roughness) {
    float sigma2 = sqrt(roughness);
    float a = 1.0f - 0.5f * sigma2 / (sigma2 + 0.33f);
    float b = 0.45f * sigma2 / (sigma2 + 0.09f);

    float theta_r = acos(dot(V, N));
    float3 VN = normalize(V-N*dot(V, N));

    float color;

    float cos_theta_i = dot(L, N);
    float cos_phi_diff = dot(VN, normalize(L - N * cos_theta_i));
    float theta_i = acos(cos_theta_i);
    float alpha = max(theta_i, theta_r);
    float beta = min(theta_i, theta_r);
    color += Kd * cos_theta_i * (a + b * max(0.0f, cos_phi_diff) * sin(alpha) * tan(beta));

    return clamp(color, 0.0f, 59999.0f);
}

inline float cookTorrance(float3 Nn, float3 In, float3 L, float Ks, float roughness) {
    float m2 = sqrt(roughness);
    float3 Vn = normalize(In);
    float3 Nf = faceforward(Nn, -Vn, Nn);

    float Ct;

    float3 Ln = normalize(L);
    float3 Hn = normalize(Vn + Ln);
    float t = dot(Hn, Nn);
    float t2 = t*t;
    float v = dot(Vn, Nn);
    float vp = dot(Ln, Nn);
    float u = dot(Hn, Vn);

    float D = 0.5f / (m2*t2*t2) * exp( (t2-1.0f) / (m2*t2) );
    float G = min(1.0f, 2.0f * min( t * v/u, t * vp/u));
    Ct += 1.0f * (Ks * D * G / (vp * v) ) * dot(Nn, Ln);

    return clamp(Ct, 0.0f, 59999.0f);

}


kernel Relighting : ImageComputationKernel<ePixelWise>
{
  Image<eRead, eAccessPoint, eEdgeNone> src;
  Image<eRead, eAccessPoint, eEdgeNone> P;
  Image<eRead, eAccessPoint, eEdgeNone> N;

  Image<eWrite> dst;

param:
  float3 light_dir;
  float3 light_pos;
  float3 light_rot;
  float4 diffC;
  float4 specC;
  float4 Ka;
  float Kd;
  float roughD;
  bool enableAniso;
  float Ks;
  float Krough;
  bool enableFresnel;
  float fbias;
  float eta;
  float Kfr;
  bool textureMult;
  float textureMix;

  void define()
  {
    defineParam(light_dir, "Light Direction", float3(0.0f, 0.0f, 1.0f));
    defineParam(light_pos, "Light Position", float3(0.0f, 0.0f, 0.0f));
    defineParam(light_rot, "Light Rotation", float3(0.0f, 45.0f, 0.0f));
    defineParam(diffC, "Diffuse Color", float4(1.0f, 1.0f, 1.0f, 1.0f));
    defineParam(specC, "Specular Color", float4(1.0f, 1.0f, 1.0f, 1.0f));
    defineParam(Ka, "Ambient", float4(0.1f, 0.1f, 0.1f, 1.0f));
    defineParam(Kd, "Diffuse Strength", 0.8f);
    defineParam(roughD, "Diffuse Roughness", 1.0f);
    defineParam(enableAniso, "Enable Anisotropic Specular", false);
    defineParam(Ks, "Specular Strength", 1.0f);
    defineParam(Krough, "Specular Roughness", 1.0f);
    defineParam(enableFresnel, "Enable Fresnel", false);
    defineParam(fbias, "Fresnel Bias", 0.15f);
    defineParam(eta, "Fresnel Refraction Index", 0.25f);
    defineParam(Kfr, "Fresnel Strength", 1.0f);
    defineParam(textureMult, "Texture Mult", false);
    defineParam(textureMix, "Texture Mix", 0.0f);
  }


  void process()
  {
    SampleType(src) result(0.0f);

//  Color
    float3 color = float3(src(0), src(1), src(2));

//  Position
    float3 point = float3(P(0), P(1), P(2));

    float3 np = normalize(point);

    float3 L = light_pos - point;

    float3 nl = normalize(L);


//  Normals
    float3 normals = float3(N(0), N(1), N(2));

    float3 nn = normalize(normals);

//    Calculate diffuse model
    float3 I = dot(nl, nn);
    float3 In = normalize(I);
    float diff = orenNayar(np, nn, I, nl, Kd, roughD);

//  Calculate Spec
    float spec = cookTorrance(nn, In, L, Ks, Krough);

    float fresnel = schlickFresnel(point, normals, I, fbias, eta, Kfr);

    if (textureMult) {
        result = Ka + (diff * diffC + spec * specC) * float4(color.x, color.y, color.z, 0.0f);
    } else {
        result = Ka + (diff * diffC + spec * specC);
    }

    result += float4(color.x, color.y, color.z, 0.0f) * textureMix;

    if (enableFresnel) result *= fresnel;

    dst() = result;
    if (src(3) > 0) {
        dst(3) = src(3);
    } else {
        dst(3) = 0;
    }
  }

};