// This is helper module with various formulas adapted from different sources

// Fractionals
inline float fract( float v )
{
    return v - floor(v);
}

inline float2 fract( float2 v )
{
    return v - floor(v);
}

inline float3 fract3( float3 v )
{
    return v - floor(v);
}


inline float4 fract4( float4 v )
{
    return v - floor(v);
}

// Linear Interpolation
// Adaptem from: 
inline float lerp(float t, float a, float b)
{
	return t <= a ? 1 : t > b ? 0 : (b - t)/(b - a);
}

// fastfloor
// https://www.codeproject.com/Tips/700780/Fast-floor-ceiling-functions
inline int fastfloor( float x )
{
	if( x > 0.0f ) return (int)(x);
	else return (int)(x - 1);
};

// smoothstep
// Adapted from: https://developer.download.nvidia.com/cg/smoothstep.html
inline float smoothstep(float edge0, float edge1, float x)
{
    float t = clamp((x - edge0) / (edge1 - edge0), 0.0f, 1.0f);
    return float(t * t * (3.0f - 2.0f * t));
}

// mix
// https://thebookofshaders.com/glossary/?search=mix
inline float mix(float from, float to, float factor)
{
    return factor * to + (1.0 - factor) * from;
}

// posterize
// https://rosenzweig.io/blog/monotone-portraits-with-glsl.html
inline float4 posterize(float4 color, float num_colors)
{
    return floor(color * num_colors - 0.5f) / num_colors;
}

// quantize
inline float2 quantize(float2 v, float steps)
{
    return floor(v * steps) / steps;
}

// Degrees to Radians
inline float deg2rad(float angle)
{
    return(angle/(180.0f/PI));
}

// Radians to Degrees
inline float rad2deg(float radian)
{
    return(radian * (180.f / PI));
}

// Hashing 
inline float4 hash42(float2 p)
{
    float4 p4 = fract4(float4(p.x, p.y, p.x, p.y) * float4(443.8975f, 397.2973f, 491.1871f, 470.7827f));
    p4 += dot(float4(p4.w, p4.z, p4.x, p4.y), p4+19.19f);
    return fract4(float4(p4.x * p4.y, p4.x * p4.z, p4.y * p4.w, p4.x * p4.w));
}

// RGB to Luminance
float luminance(float3 in) {
	float l = 0.299f*in.x + 0.587f*in.y + 0.114f*in.z;

	return l;
}

// RGB to HSP
// Adapted from: http://alienryderflex.com/hsp.html
// by Darel Rex Finley
float3 rgb2hsp(float3 in) {
    float3 out;
    out.z = sqrt(in.x*in.x*0.299f+in.y*in.y*0.587f+in.z*in.z*0.114f);

    if( in.x == in.y && in.x == in.z) {
        out.x = 0;
        out.y = 0;
        return out;
    }
    if( in.x >= in.y && in.x >= in.z ) { // R is largest
        if( in.z >= in.y ) {
            out.x = 6.0f/6.0f-1.0f/6.0f*(in.z-in.y)/(in.x-in.y);
            out.y = 1.0f-in.y/in.x;
        } else {
            out.x = 0.0f/6.0f+1.0f/6.0f*(in.y-in.z)/(in.x-in.z);
            out.y = 1.0f-in.z/in.x;
        }
    } else // G is largest
    if( in.y >= in.x && in.y >= in.z ) {
        if( in.x >= in.z ) {
            out.x = 2.0f/6.0f-1.0f/6.0f*(in.x-in.z)/(in.y-in.z);
            out.y = 1.0f-in.z/in.y;
        } else {
            out.x = 2.0f/6.0f-1.0f/6.0f*(in.z-in.x)/(in.y-in.x);
            out.y = 1.0f-in.x/in.y;
        }
    } else // B is largest
    if( in.y >= in.x ) {
        out.x = 4.0f/6.0f-1.0f/6.0f*(in.y-in.x)/(in.z-in.x);
        out.y = 1.0f-in.x/in.z;
    } else {
        out.x = 4.0f/6.0f-1.0f/6.0f*(in.x-in.y)/(in.z-in.y);
        out.y = 1.0f-in.y/in.z;
    }

    return out;

}


// RGB to HSV
// Adapted from: http://code.activestate.com/recipes/576919-python-rgb-and-hsv-conversion/
// and: https://hagbarth.net/creating-a-viewer-interactive-nuke-gizmo/
float3 rgb2hsv(float3 in) {
    float3 out;
    float      min, max, delta;
    min = in.x < in.y ? in.x : in.y;
    min = min  < in.z ? min  : in.z;
    max = in.x > in.y ? in.x : in.y;
    max = max  > in.z ? max  : in.z;
    out.z = max;
    delta = max - min;
    if( max > 0.0 ) {
        out.y = (delta / max);
    } else {
        out.y = 0.0;
        out.x = 0.0;
        return out;
    }
    if( in.x >= max )
        out.x = ( in.y - in.z ) / delta;
    else
    if( in.y >= max )
        out.x = 2.0 + ( in.z - in.x ) / delta;
    else
        out.x = 4.0 + ( in.x - in.y ) / delta;

    out.x *= 60.0;

    if( out.x < 0.0 )
        out.x += 360.0;

    return out;
}

// HSV to RGB
// Adapted from: http://code.activestate.com/recipes/576919-python-rgb-and-hsv-conversion/
// and: https://hagbarth.net/creating-a-viewer-interactive-nuke-gizmo/
float3 hsv2rgb(float3 in) {
    float      hh, p, q, t, ff;
    int        i;
    float3         out;

    if(in.y <= 0.0) {
        out.x = in.z;
        out.y = in.z;
        out.z = in.z;
        return out;
    }
    hh = in.x;
    if(hh >= 360.0) hh = 0.0;
    hh /= 60.0;
    i = (int)hh;
    ff = hh - i;
    p = in.z * (1.0 - in.y);
    q = in.z * (1.0 - (in.y * ff));
    t = in.z * (1.0 - (in.y * (1.0 - ff)));

    if (i == 0) {
        out.x = in.z;
        out.y = t;
        out.z = p;
    }
    else if (i == 1) {
        out.x = q;
        out.y = in.z;
        out.z = p;
    }
    else if (i == 2) {
        out.x = p;
        out.y = in.z;
        out.z = t;
    }
    else if (i == 3) {
        out.x = p;
        out.y = q;
        out.z = in.z;
    }
    else if (i ==4) {
        out.x = t;
        out.y = p;
        out.z = in.z;
    }
    else if (i == 5) {
        out.x = in.z;
        out.y = p;
        out.z = q;
    }
    return out;
}
